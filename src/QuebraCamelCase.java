import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class QuebraCamelCase {

	public static List<String> converterCamelCase(String original) {
		
		if (original.isEmpty()) {
			throw new PalavraVaziaException();
		}
		
		String specialChars = ".*[`~!@#$%^&*()\\-_=+\\\\|\\[{\\]};:'\",<.>/?].*";
		if (Pattern.compile(specialChars).matcher(original).matches()) {
			throw new CaracterNaoPermitidoException();
		}

		List<String> palavrasProcessadas = new ArrayList<>();
		String palavraEmProcessamento = "";

		for (int posicao = 0; posicao < original.length(); posicao++) {
			char caractereCorrente = original.charAt(posicao);
			
			if(ehNumero(caractereCorrente)) {
				if (ehPosicaoInicial(posicao)) throw new NaoComecarComNumeroException();
				if (jaIniciouONumero(palavraEmProcessamento)) {
					palavraEmProcessamento = adicionaLetraNaPalavra(palavraEmProcessamento, caractereCorrente);
					continue;
				}
				palavraEmProcessamento = iniciaPalavra(palavrasProcessadas, palavraEmProcessamento, caractereCorrente, false);
			}

			if (ehLetraMinuscula(caractereCorrente)) {
				palavraEmProcessamento = adicionaLetraNaPalavra(palavraEmProcessamento, caractereCorrente);
				continue;
			}

			if (ehLetraMaiuscula(caractereCorrente)) {
				if (ehSigla(original, posicao)) {
					if (jaIniciouASigla(palavraEmProcessamento)) {
						palavraEmProcessamento = adicionaLetraNaPalavra(palavraEmProcessamento, caractereCorrente);
						continue;
					}
					palavraEmProcessamento = iniciaPalavra(palavrasProcessadas, palavraEmProcessamento, caractereCorrente, true);
					continue;
				}
				palavraEmProcessamento = iniciaPalavra(palavrasProcessadas, palavraEmProcessamento, caractereCorrente, false);
			}
		}

		palavrasProcessadas.add(palavraEmProcessamento);
		return palavrasProcessadas;

	}
	
	private static boolean temPalavraEmProcessamento(String palavraEmProcessamento) {
		return !palavraEmProcessamento.isEmpty();
	}

	private static void guardaPalavraAnterior(List<String> palavras, String palavra) {
		if (temPalavraEmProcessamento(palavra)) {
			palavras.add(palavra);
		}
	}
	
	private static String iniciaNovaPalavra(char letra, boolean isUpperCase) {
		if (isUpperCase) {
			return String.valueOf(letra);
		}
		return String.valueOf(letra).toLowerCase();
	}

	private static String iniciaPalavra(List<String> palavrasProcessadas, String palavraEmProcessamento,
			char caractereCorrente, boolean ehSigla) {
		guardaPalavraAnterior(palavrasProcessadas, palavraEmProcessamento);
		return iniciaNovaPalavra(caractereCorrente, ehSigla);
	}

	private static String adicionaLetraNaPalavra(String palavra, char letra) {
		return palavra + String.valueOf(letra);
	}
	
	private static boolean ehNumero(char letraCorrente) {
		return Character.isDigit(letraCorrente);
	}

	private static boolean ehLetraMaiuscula(char letra) {
		return Character.isUpperCase(letra);
	}

	private static boolean ehLetraMinuscula(char letra) {
		return Character.isLowerCase(letra);
	}

	private static boolean ehUltimaLetra(String original, int i) {
		return (i == original.length() - 1);
	}
	
	private static boolean ehSigla(String original, int i) {
		if (ehUltimaLetra(original, i))
			return true;
		char proximoCaractere = original.charAt(i + 1);
		return ehLetraMaiuscula(proximoCaractere);
	}
	
	private static boolean ehPosicaoInicial(int posicao) {
		return posicao == 0;
	}
	
	private static boolean jaIniciouONumero(String palavraEmProcessamento) {
		String digits = "[0-9]";
		return Pattern.compile(digits).matcher(palavraEmProcessamento).matches();
	}
	
	private static boolean jaIniciouASigla(String palavraEmProcessamento) {
		return palavraEmProcessamento.equals(palavraEmProcessamento.toUpperCase());
	}

}
