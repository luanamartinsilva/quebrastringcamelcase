import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class QuebraCamelCaseTest {
	
	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void testaPalavraVazia() {
		exception.expect(PalavraVaziaException.class);
		QuebraCamelCase.converterCamelCase("");
	}
	
	@Test
	public void testaUmaPalavraMinuscula() {
		List<String> palavras = QuebraCamelCase.converterCamelCase("nome");
		assertEquals(new ArrayList<String>(Arrays.asList("nome")), palavras);
	}
	
	@Test
	public void testaUmaPalavraMaiuscula() {
		List<String> palavras = QuebraCamelCase.converterCamelCase("Nome");
		assertEquals(new ArrayList<String>(Arrays.asList("nome")), palavras);
	}
	
	@Test
	public void testaDuasPalavras() {
		List<String> palavras = QuebraCamelCase.converterCamelCase("nomeComposto");
		assertEquals(new ArrayList<String>(Arrays.asList("nome", "composto")), palavras);
	}
	
	@Test
	public void testaUmaPalavraComSigla() {
		List<String> palavras = QuebraCamelCase.converterCamelCase("CPF");
		assertEquals(new ArrayList<String>(Arrays.asList("CPF")), palavras);
	}
	
	@Test
	public void testaDuasPalavrasComSiglaNoFinal() {
		List<String> palavras = QuebraCamelCase.converterCamelCase("numeroCPF");
		assertEquals(new ArrayList<String>(Arrays.asList("numero","CPF")), palavras);
	}
	
	@Test
	public void testaDuasPalavrasComSiglaNoComeço() {
		List<String> palavras = QuebraCamelCase.converterCamelCase("CPFNumero");
		assertEquals(new ArrayList<String>(Arrays.asList("CPF","numero")), palavras);
	}
	
	@Test
	public void testaTresPalavrasComSiglaNoMeio() {
		List<String> palavras = QuebraCamelCase.converterCamelCase("numeroCPFContribuinte");
		assertEquals(new ArrayList<String>(Arrays.asList("numero","CPF","contribuinte")), palavras);
	}
	
	@Test
	public void testaPalavraComCaracterNaoPermitido() {
		exception.expect(CaracterNaoPermitidoException.class);
		QuebraCamelCase.converterCamelCase("nome#Composto");
	}
	
	@Test
	public void testaPalavrasComNumeroNoComeco() {
		exception.expect(NaoComecarComNumeroException.class);
		QuebraCamelCase.converterCamelCase("10Primeiros");
	}
	
	@Test
	public void testaTresPalavrasComNumeroNoMeio() {
		List<String> palavras = QuebraCamelCase.converterCamelCase("recupera10Primeiros");
		assertEquals(new ArrayList<String>(Arrays.asList("recupera","10","primeiros")), palavras);
	}
	
	@Test
	public void testaTresPalavrasComNumeroNoFinal() {
		List<String> palavras = QuebraCamelCase.converterCamelCase("recuperaPrimeiros10");
		assertEquals(new ArrayList<String>(Arrays.asList("recupera","primeiros", "10")), palavras);
	}
	
	

}
